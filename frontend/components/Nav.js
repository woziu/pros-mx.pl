import Link from 'next/link';
import NavStyles from './styles/NavStyles';

const Nav = () => (
  <NavStyles>
    <Link href='/items'>
      <a>Sklep</a>
    </Link>
    <Link href='/sell'>
      <a>Dodaj produkt</a>
    </Link>
    <Link href='/orders'>
      <a>Zamówienia</a>
    </Link>
    <Link href='/account'>
      <a>Konto</a>
    </Link>
    <Link href='/signup'>
      <a>Rejestracja</a>
    </Link>
  </NavStyles>
);

export default Nav;
