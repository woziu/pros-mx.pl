import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Router from 'next/router';

import Form from './styles/Form';
import formatMoney from '../lib/formatMoney';
import Error from './ErrorMessage';

const CREATE_ITEM_MUTATION = gql`
  mutation CREATE_ITEM_MUTATION(
    $title: String!
    $description: String!
    $image: String
    $largeImage: String
    $price: Int!
  ) {
    createItem(
      title: $title
      description: $description
      image: $image
      largeImage: $largeImage
      price: $price
    ) {
      id
    }
  }
`;

class CreateItem extends Component {
  state = {
    title: '',
    description: '',
    image: '',
    largeImage: '',
    price: 0
  };

  handleChange = e => {
    const { name, type, value } = e.target;
    const val = type === 'number' ? parseFloat(value) : value;
    this.setState({
      [name]: val
    });
  };

  uploadFile = async e => {
    const files = e.target.files;
    const data = new FormData();
    data.append('file', files[0]);
    data.append('upload_preset', 'prosmoto');

    const res = await fetch(
      'https://api.cloudinary.com/v1_1/dznhzo0hc/image/upload',
      {
        method: 'POST',
        body: data
      }
    );
    const file = await res.json();
    console.log(file);
    this.setState({
      image: file.secure_url,
      largeImage: file.eager[0].secure_url
    });
  };

  render() {
    return (
      <Mutation mutation={CREATE_ITEM_MUTATION} variables={this.state}>
        {(createItem, { loading, error }) => {
          return (
            <Form
              onSubmit={async e => {
                e.preventDefault();
                const res = await createItem();
                Router.push({
                  pathname: '/item',
                  query: { id: res.data.createItem.id }
                });
              }}
            >
              <Error error={error} />
              <fieldset disabled={loading} aria-busy={loading}>
                <label htmlFor='file'>
                  Obrazek
                  <input
                    type='file'
                    name='file'
                    id='file'
                    placeholder='Załaduj obrazek'
                    required
                    onChange={this.uploadFile}
                  />
                  {this.state.image && (
                    <img src={this.state.image} alt='Upload Preview' />
                  )}
                </label>
                <label htmlFor='title'>
                  Tytuł
                  <input
                    type='text'
                    name='title'
                    id='title'
                    placeholder='Tytuł'
                    required
                    value={this.state.title}
                    onChange={this.handleChange}
                  />
                </label>
                <label htmlFor='price'>
                  Cena
                  <input
                    type='number'
                    name='price'
                    id='price'
                    placeholder='Cena'
                    required
                    value={this.state.price}
                    onChange={this.handleChange}
                  />
                </label>
                <label htmlFor='description'>
                  Opis
                  <textarea
                    type='text'
                    name='description'
                    id='description'
                    placeholder='Opis'
                    required
                    value={this.state.description}
                    onChange={this.handleChange}
                  />
                </label>
                <button type='submit'>Wyślij</button>
              </fieldset>
            </Form>
          );
        }}
      </Mutation>
    );
  }
}

export default CreateItem;
export { CREATE_ITEM_MUTATION };
